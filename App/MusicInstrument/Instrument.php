<?php
declare(strict_types=1);

namespace App\MusicInstrument;


abstract class Instrument {

    protected $name;
    protected $manufacturer;
    protected $price;

    abstract public function setName(string $name);
    abstract public function getName();

    abstract public function setManufacturer(string $manufacturer);
    abstract public function getManufacturer();

    abstract public function setPrice(float $price);
    abstract public function getPrice();





}