<?php
declare(strict_types=1);
namespace App\MusicInstrument;
require_once('vendor/autoload.php');
//require_once ('App/MusicInstrument/Instrument.php');


class Guitars extends Instrument
{
    private $type;
    private $numStrings;



    /* public function setType(string $type): Guitars
    {
        $this->type = $type;
        return $this;
    } */

    public function __get($type): string
    {
        return $this->type = "Bass";
    }

    public function setNumStrings(int $numStrings): Guitars
    {
        $this->numStrings = $numStrings;
        return $this;
    }

    private function getNumStrings(): int
    {
        return $this->numStrings;
    }

    public function __call($getNumStrings, $args)
    {
        echo "The method is private!";
    }


    public function setPrice(float $price): Instrument
    {
        $this->price = $price;
        return $this;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setManufacturer(string $manufacturer): Instrument
    {
        $this->manufacturer = $manufacturer;
        return $this;
    }

    public function getManufacturer(): string
    {
        return $this->manufacturer;
    }

    public function setName(string $name): Instrument
    {
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }
}