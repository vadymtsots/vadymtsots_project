<?php
namespace App\MusicInstrument;

require_once('App/MusicInstrument/Instrument.php');

class Keys extends Instrument {
    protected $type;

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setPrice(float $price): Instrument
    {
        $this->price = $price;
        return $this;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setManufacturer(string $manufacturer): Instrument
    {
        $this->manufacturer = $manufacturer;
        return $this;
    }

    public function getManufacturer(): string
    {
        return $this->manufacturer;
    }

    public function setName(string $name): Instrument
    {
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
